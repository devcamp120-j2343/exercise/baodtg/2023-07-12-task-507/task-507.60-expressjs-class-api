//Khai báo thư viện express
const express = require('express');
const { companyRouter } = require('./app/companyRouter');

//khơi tạo express app
const app = express()

//Khởi tạo cổng port:
const port = 8000;

app.use("/", companyRouter);


app.listen(port, () => {
    console.log(`App listen on port ${8000}`)
})