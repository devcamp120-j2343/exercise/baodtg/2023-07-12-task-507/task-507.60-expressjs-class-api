//Khai báo thư viện express:
const express = require('express');

//khoi tao router
const companyRouter = express.Router();

class Company {
    constructor(id, company, contact, country) {
        this.id = id,
            this.company = company,
            this.contact = contact,
            this.country = country
    }
};
const companiesArr = [
    new Company(1, "Alfreds Futterkiste", "Maria Anders", "Germany"),
    new Company(2, "Centro comercial Moctezuma", "Francisco Chang", "Mexico"),
    new Company(3, "Ernst Handel", "Roland Mendel", "Austria"),
    new Company(4, "Island Trading", "Helen Bennett", "UK"),
    new Company(5, "Laughing Bacchus Winecellars", "Yoshi Tannamuri", "Canada"),
    new Company(6, "Magazzini Alimentari Riuniti", "Giovanni Rovelli", "Italy")
];
//companyRouter GET All company
companyRouter.get('/companies', (req, res) => {
    

    console.log(`Get all companies`);
    res.json({
        companiesArr
    })
})

//companyRouter GET company by id
companyRouter.get('/companies/:companyId', (req, res) => {
    const companyId = req.params.companyId;
    console.log(`Get company by id: ${companyId}`);

    if (companyId != null) {
        let getCompanyById = companiesArr.filter(companiesArr => companiesArr.id == companyId);
        res.json(
            getCompanyById
        )
    }

})

//companyRouter Post company (create company)
companyRouter.post('/companies', (req, res) => {
    console.log(`Create company`);
    res.json({
        message: `Create company`
    })
})

//companyRouter PUT company (update company)
companyRouter.put('/companies/:companyId', (req, res) => {
    const companyId = req.params.companyId;
    console.log(`Update company by id: ${companyId}`);

    res.json({
        message: `Update company by id: ${companyId}`
    })
})

//companyRouter PUT company (delete company)
companyRouter.delete('/companies/:companyId', (req, res) => {
    const companyId = req.params.companyId;
    console.log(`Delete company by id: ${companyId}`);

    res.json({
        message: `Delete company by id: ${companyId}`
    })
})

module.exports = { companyRouter }